# -*- coding: utf-8 -*-
import os
import random
import logging
from blockchain_network.blockchainNetwork import BlockchainNetwork


if __name__ == '__main__':
    log_name = "logs/cpu_vs_winning.log"
    logging.basicConfig(
        filename=log_name,
        format='%(asctime)s %(levelname)s: %(message)s',
        level=logging.INFO, datefmt="%H:%M:%S",
        filemode='w'
    )
    for II in range(100):
        plot_graphs = False
        n_node = 100
        p_edge = 0.1
        p_back_bone = 0
        time_limit = 10
        p_mining = 0.1

        try:
            b = BlockchainNetwork(
                n_node,
                p_edge,
                p_back_bone,
                time_limit,
                p_mining
            )
            # print(b.property_node(1))
            # b.plot_graph()
            if not b.is_connected():
                print("Disconnected graph!")
                continue

            filename = "./results/test_cpu_vs_winning.csv".format()

            if (not os.path.exists(filename)) or (os.path.getsize(filename) < 5):
                myfile = open(filename, "w")
                myfile.write("n node; p_edge;")

                kpis = [
                    'n_neighbors',
                    'n_neighbors_2',
                    'mean_dist_other_node',
                    'max_dist_other_node',
                    'median_dist_other_node',
                    'std_dev_dist_other_node',
                    'closeness_centrality'
                ]

                for ele in kpis:
                    myfile.write("{}_0;".format(ele))
                for ele in kpis:
                    myfile.write("{}_1;".format(ele))
                myfile.write("winning;\n")
            else:
                myfile = open(filename, "a")

            winning_miners = random.sample(range(n_node), 2)

            # impongo due nodi a caso che fanno i miners
            b.set_winning_miners(winning_miners)
            # faccio partire la propagazione

            # b.load_simulation()
            b.run_simulation(8, plot_graph=False)
            consensus_flag, block = b.consensus()
            print(winning_miners)
            print(block)
            winning = int(block.split('-')[1])

            if winning_miners[0] == winning:
                winning = 0
            else:
                winning = 1
            prop_node_0 = b.property_node(winning_miners[0])
            prop_node_1 = b.property_node(winning_miners[1])

            myfile.write(
                "{}; {}; {}; {}; {}\n".format(
                    n_node, p_edge,
                    ";".join(str(i) for i in prop_node_0),
                    ";".join(str(i) for i in prop_node_1),
                    winning,
                )
            )
            myfile.close()
        except Exception as e:
            print("PROBLEM: {}".format(e))
    # os.system('espeak -s 120 -v it "beep"')
