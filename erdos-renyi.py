# -*- coding: utf-8 -*-
import networkx as nx
import numpy as np
import sys
import random
import operator
import logging
import pickle
from plot_result import graphs
from competing_tipping import competing_tipping

if __name__ == '__main__':
    log_name = "logs/erdos-renyi.log"
    logging.basicConfig(
        filename=log_name,
        format='%(asctime)s %(levelname)s: %(message)s',
        level=logging.INFO, datefmt="%H:%M:%S",
        filemode='w'
    )


    n_node = 15
    p = 0.5
    g = nx.erdos_renyi_graph(n_node, p)
    seed_set_red = {0}
    seed_set_blue = {n_node - 1}
    '''
    # TIME EXPERIMENTS
    # Graph creation
    consensus = competing_tipping.competing_tipping(g, 0,
                                                    seed_set_red,
                                                    seed_set_blue, 0, 0,
                                                    set_weight=True)

    consensus.plot_graph()
    active_red, active_blue = consensus.run(print_graphs=True)


    consensus.dump_video()
    '''


    consensus = competing_tipping.competing_tipping(
        g, 0,
        seed_set_red,
        seed_set_blue
    )


    # consensus.plot_graph()
    # matrix = consensus.get_dijkstra_matrix()
    # print(consensus.get_n_conn_red_2())
    # print(consensus.get_n_conn_blue_2())


    consensus.plot_graph()
    active_red, active_blue = consensus.run(print_graphs=True)

    consensus.dump_video()

    # eigs = consensus.get_eigen_values_distance()
    # print(max(abs(eigs)))
    # print(min(abs(eigs)))


    # print("red:   {}".format(consensus.get_n_conn_red()))
    # print("blue:  {}".format(consensus.get_n_conn_blue()))


    '''
    # consensus.print_graph()
    active_red, active_blue = consensus.run(print_graphs=False)

    dijkstra_matrix = consensus.get_dijkstra_matrix()
    '''

    # index, value = max(enumerate({lr,lb}), key=operator.itemgetter(1))
    # print("{},{},{}".format(index, lr, lb))

    '''
    if index == 0:
        c_max = cc[list(seed_set_red)[0]]
        c_min = cc[list(seed_set_blue)[0]]
    else:
        c_max = cc[list(seed_set_blue)[0]]
        c_min = cc[list(seed_set_red)[0]]

    D = 0
    for k in range(0,99):
        nx.shortest_path(g,list(seed_set_red)[0],k)
        D = D + np.sign( nx.shortest_path_length(g,list(seed_set_red)[0],k) - nx.shortest_path_length(g,list(seed_set_blue)[0],k))

    if index == 1:
        D = -D

    lmax = max(lr, lb)
    lmin = min(lr,lb)
    lb = g.number_of_nodes() - lmax - lmin

    print("{}, {}, {}, {}, {}, {}, {} ".format(time_steps, lmax, lmin, lb, c_max, c_min, D))

    nx.draw(g, pos, node_size=10, node_color='yellow', font_size=8, font_weight='bold')
    plt.savefig("Graph.png", format="PNG")
    '''
