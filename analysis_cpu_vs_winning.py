# -*- coding: utf-8 -*-
import os
import random
import logging
import pandas as pd


if __name__ == '__main__':
    log_name = "logs/analysis_cpu_vs_winning.log"
    df = pd.read_csv(
        './results/test_cpu_vs_winning.csv',
        sep=';',
        header=0
    )
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    tot_rows = len(df)

    # print(df.columns)
    print("tot_rows: {}".format(tot_rows))

    n_neighbors = len(df[
        (
            (df.n_neighbors_0 > df.n_neighbors_1) & \
            (df.winning == 0)
        ) |
            (df.n_neighbors_1 > df.n_neighbors_0) & \
            (df.winning == 1)
    ])
    print("neighbors: {}".format(
        (n_neighbors + 0.0) / (tot_rows + 0.0))
    )

    n_neighbors_2 = len(df[
        (
            (df.n_neighbors_2_0 > df.n_neighbors_2_1) & \
            (df.winning == 0)
        ) |
            (df.n_neighbors_2_1 > df.n_neighbors_2_0) & \
            (df.winning == 1)
    ])
    print("neighbors_2: {}".format(
        (n_neighbors_2 + 0.0) / (tot_rows + 0.0))
    )

    n_mean_dist_other_node = len(df[
        (
            (df.mean_dist_other_node_0 < df.mean_dist_other_node_1) & \
            (df.winning == 0)
        ) |
            (df.mean_dist_other_node_1 < df.mean_dist_other_node_0) & \
            (df.winning == 1)
    ])
    print("mean_dist_other_node: {}".format(
        (n_mean_dist_other_node + 0.0) / (tot_rows + 0.0))
    )

    n_max_dist_other_node = len(df[
        (
            (df.max_dist_other_node_0 < df.max_dist_other_node_1) & \
            (df.winning == 0)
        ) |
            (df.max_dist_other_node_1 < df.max_dist_other_node_0) & \
            (df.winning == 1)
    ])
    print("max_dist_other_node: {}".format(
        (n_max_dist_other_node + 0.0) / (tot_rows + 0.0))
    )

    n_median_dist_other_node = len(df[
        (
            (df.median_dist_other_node_0 < df.median_dist_other_node_1) & \
            (df.winning == 0)
        ) |
            (df.median_dist_other_node_1 < df.median_dist_other_node_0) & \
            (df.winning == 1)
    ])
    print("median_dist_other_node: {}".format(
        (n_median_dist_other_node + 0.0) / (tot_rows + 0.0))
    )


    n_closeness_centrality = len(df[
        (
            (df.closeness_centrality_0 > df.closeness_centrality_1) & \
            (df.winning == 0)
        ) |
            (df.closeness_centrality_1 > df.closeness_centrality_0) & \
            (df.winning == 1)
    ])
    print("closeness_centrality: {}".format(
        (n_closeness_centrality + 0.0) / (tot_rows + 0.0))
    )
