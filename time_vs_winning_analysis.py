# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt


if __name__ == '__main__':
    log_name = "logs/time_vs_winning_analysis.log"
    df = pd.read_csv(
        './results/test_time_vs_winning.csv',
        sep=';',
        header=0
    )
    colors = ["red", "blue", "green"]
    path_fig = None
    # ###############
    # delta_t vs p_winning
    # ###############
    delta_t_values = df.delta_t.unique()
    p_values = df.p.unique()
    p_values.sort()
    print("p_values: {}".format(p_values))

    for i, delta_t in enumerate(delta_t_values):
        print("delta_t: {}".format(delta_t))
        y_vals = []
        for p_edge in p_values:
            tot = len(df.loc[(df['p'] == p_edge) & (df['delta_t'] == delta_t)])
            first_win = len(df.loc[(df['p'] == p_edge) & (df['delta_t'] == delta_t) & (df['winner'] == 0)])
            y_vals.append(float(first_win) / float(tot))
            print(
                "\t p_edge: {} -- {}/{} [{}]".format(
                    p_edge,
                    first_win,
                    tot,
                    y_vals[-1]
                )
            )
        plt.plot(
            p_values, y_vals,
            color=colors[i], label="delta t: {}".format(delta_t)
        )

    plt.title("Probability first wins")
    plt.xlabel('p edge')
    plt.ylabel('p first wins')
    plt.legend()
    if path_fig:
        plt.savefig(
            '{}/delta_t_winning.png'.format(
                path_fig
            )
        )
    else:
        plt.show()
    plt.close()
