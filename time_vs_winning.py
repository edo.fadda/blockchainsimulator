# -*- coding: utf-8 -*-
import os
import random
import logging
from blockchain_network.blockchainNetwork import BlockchainNetwork


if __name__ == '__main__':
    log_name = "logs/time_vs_winning.log"
    logging.basicConfig(
        filename=log_name,
        format='%(asctime)s %(levelname)s: %(message)s',
        level=logging.INFO, datefmt="%H:%M:%S",
        filemode='w'
    )

    filename = "./results/test_time_vs_winning.csv".format()
    if (not os.path.exists(filename)) or (os.path.getsize(filename) < 5):
        myfile = open(filename, "w")
        myfile.write("N;p;delta_t;max_cc;min_cc;mean_cc;std_cc;winner;cc_first;cc_second\n")
    else:
        myfile = open(filename, "a")

    p_edge = 0.08
    plot_graphs = False
    n_node = 100
    p_back_bone = 0
    time_limit = 10
    p_mining = 0.15

    delta_t = 2
    time_limit_simulation = 8

    for p_edge in [0.05, 0.06, 0.07, 0.08, 0.09, 0.1]:
        for i in range(50):
            b = BlockchainNetwork(
                n_node,
                p_edge,
                p_back_bone,
                time_limit,
                p_mining
            )
            if not b.is_connected():
                print("Disconnected graph!")
                continue
            winning_miners = random.sample(range(n_node), 2)
            winning_miners_dict = {
                1: [winning_miners[0]],
                1 + delta_t: [winning_miners[1]]
            }
            b.set_winning_miners_time(winning_miners_dict, time_limit_simulation)

            b.run_simulation(time_limit_simulation, plot_graph=False)

            consensus_flag, block = b.consensus(0.95)

            if consensus_flag:
                myfile.write(
                    "{};{};{};".format(
                        n_node,
                        p_edge,
                        delta_t)
                )
                winning = int(block.split('-')[1])

                cc_stats = b.get_cc_stats()
                for stat in cc_stats:
                    myfile.write("{};".format(stat))

                if winning_miners[0] == winning:
                    myfile.write("0;")
                else:
                    myfile.write("1;")

                cc_first = b.property_node(winning_miners[0])[-1]
                cc_second = b.property_node(winning_miners[1])[-1]

                myfile.write(
                    "{};{}\n".format(
                        cc_first,
                        cc_second)
                )

    os.system('espeak -s 120 -v it "beep"')
