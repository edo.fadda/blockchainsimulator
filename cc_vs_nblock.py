# -*- coding: utf-8 -*-
import os
import random
import logging
from blockchain_network.blockchainNetwork import BlockchainNetwork


if __name__ == '__main__':
    log_name = "logs/cpu_vs_winning.log"
    logging.basicConfig(
        filename=log_name,
        format='%(asctime)s %(levelname)s: %(message)s',
        level=logging.INFO, datefmt="%H:%M:%S",
        filemode='w'
    )

    filename = "./results/test_cc_vs_n_block.csv".format()
    if (not os.path.exists(filename)) or (os.path.getsize(filename) < 5):
        myfile = open(filename, "w")
        myfile.write("N;p;max_cc;min_cc;mean_cc;std_cc;q_node_0;q_node_1;conv;det\n")
    else:
        myfile = open(filename, "a")
    for p_edge in [0.05, 0.06, 0.07, 0.08, 0.09, 0.1]:
        for i in range(20):
            plot_graphs = False
            n_node = 100
            p_back_bone = 0
            time_limit = 10
            p_mining = 0.1
            b = BlockchainNetwork(
                n_node,
                p_edge,
                p_back_bone,
                time_limit,
                p_mining
            )

            if not b.is_connected():
                print("Disconnected graph!")
                continue

            winning_miners = random.sample(range(n_node), 2)
            time_limit_simulation = 15

            b.set_winning_miners(winning_miners, time_limit_simulation)
            b.run_simulation(time_limit_simulation, plot_graph=False)
            b.log_graph()
            consensus_flag, block = b.consensus(0.95)
            myfile.write("{};{};".format(
                n_node,
                p_edge)
            )
            cc_stats = b.get_cc_stats()
            for stat in cc_stats:
                myfile.write("{};".format(stat))

            # quantiles:
            myfile.write("{};".format(
                b.get_cdf_cc_node(winning_miners[0]))
            )
            myfile.write("{};".format(
                b.get_cdf_cc_node(winning_miners[1]))
            )

            if consensus_flag:
                winning = int(block.split('-')[1])
                pos_winner = winning_miners.index(winning)
                myfile.write(
                    "conv;{}\n".format(
                        pos_winner)
                )
            else:
                size_fork = block
                myfile.write("no_conv;{}\n".format(
                    size_fork)
                )

    os.system('espeak -s 120 -v it "beep"')
