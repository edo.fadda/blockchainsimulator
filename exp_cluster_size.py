# -*- coding: utf-8 -*-
import networkx as nx
import numpy as np
import logging
import pickle
from plot_result import graphs
from competing_tipping import competing_tipping

'''
SCRIPT FOR CLUSTER SIZES
'''


def print_hist(l_M, l_m):
    graphs.plot_two_histograms(
        l_m, 'blue', l_M, 'red',
        "img/Hist_{}_{}.png".format(n_node, p)
    )


if __name__ == '__main__':
    log_name = "logs/exp_cluster_size.log"
    logging.basicConfig(
        filename=log_name,
        format='%(asctime)s %(levelname)s: %(message)s',
        level=logging.INFO, datefmt="%H:%M:%S",
        filemode='w'
    )

    n_node = 1000
    p = 0.1
    # for p in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
    N_EXP = 10
    load = False
    compute = True

    if not(load or compute):
        print("Error in initialization ")
        quit()

    name_file = "results/cluster_dim_{}_{}.p".format(n_node, p)

    lr = []
    lb = []
    ln = []

    if load:
        try:
            data1 = pickle.load(open(name_file, "rb"))
            ln = data1["ln"]
            lb = data1["lb"]
            lr = data1["lr"]
            print("loading data...")
            print_hist(lr, lb)
        except Exception as e:
            print("Error:{}".format(e))
            quit()
    if compute:
        try:
            data1 = pickle.load(open(name_file, "rb"))
            ln = data1["ln"]
            lb = data1["lb"]
            lr = data1["lr"]
            print("{} data already found. ".format(len(lr)))
        except Exception as e:
            print(e)
            pass

        for i in range(N_EXP):

            # Graph creation
            g = nx.erdos_renyi_graph(n_node, p)
            seed_set_red = {0}
            seed_set_blue = {n_node - 1}

            # Graph creation
            consensus = competing_tipping.competing_tipping(g, 0,
                                                            seed_set_red,
                                                            seed_set_blue)

            active_red, active_blue = consensus.run(False)
            l_red = consensus.get_size_red()
            l_blue = consensus.get_size_blue()
            l_black = n_node - lr[-1] - lb[-1]
            lr.append(l_red)
            lb.append(l_blue)
            ln.append(l_black)
            print("{}/{} {} {}".format(i, N_EXP, l_red, l_blue))

        data = {"lr": lr, "lb": lb, "ln": ln}
        pickle.dump(data, open(name_file, "wb"))
        print("Average red: {}".format(sum(lr) / len(lr)))
        print("Average blue:{} ".format(sum(lb) / len(lb)))

    # print("Average red: {}".format(sum(l_m) / len(l_m)))
    # print("Average blue:{} ".format(sum(l_M) / len(l_M)))

    l_M = []
    l_m = []
    l_n = []
    for i in range(len(lr)):
        l_M.append(max(lr[i], lb[i]))
        l_m.append(min(lr[i], lb[i]))
        l_n.append(n_node - lr[i] - lb[i])
    print_hist(l_M, l_m)


    graphs.plot_scatter_x_y(l_M, 'red', l_n, 'Black',
                            "label","img\Scatter_{}_{}.png".format(n_node, p))
