import imageio


def produce_video_short(filenames, duration_per_frame, path):
    images = []
    for filename in filenames:
        print(filename)
        images.append(imageio.imread(filename))
    imageio.mimsave('movie.gif', images, duration=0.5)


def produce_video_long(filenames, duration_per_frame, path):
    """
    filenames: list of all file names ()
    """
    file_path = path +'movie.gif'
    with imageio.get_writer(file_path, mode='I', duration=duration_per_frame) as writer:
        for filename in filenames:
            image = imageio.imread(filename)
            writer.append_data(image)
