import numpy
from matplotlib import pyplot


def plot_two_histograms(x, label_x, y, label_y, path):

    inf = min(min(x), min(y))
    sup = max(max(x), max(y))
    bins = numpy.linspace(inf, sup, 100)
    pyplot.figure()

    pyplot.hist(x, bins, alpha=1, label=label_x, normed=1,
                facecolor='blue', edgecolor='black', linewidth=0.5)
    pyplot.hist(y, bins, alpha=1, label=label_y, normed=1,
                facecolor='red', edgecolor='black', linewidth=0.5)

    pyplot.legend(loc='upper right')
    pyplot.savefig(path)
    pyplot.close()



def plot_scatter_x_y(x, label_x, y, label_y, label, path):
    pyplot.figure()
    if label == "":
        pyplot.scatter(x, y, c="g", alpha=0.5)
    else:
        pyplot.scatter(x, y, c="g", alpha=0.5, label=label)
        pyplot.legend(loc=2)

    # axis
    pyplot.axvline(x=0.0, linestyle='-', color='black', linewidth=1)
    pyplot.axhline(y=0.0, linestyle='-', color='black', linewidth=1)

    # y axis
    # y_axis_X = [min(y), max(y)]
    # y_axis_Y = [0, 0]
    # pyplot.plot(y_axis_X, y_axis_Y)

    # plot
    pyplot.grid(color='grey', linestyle='-', linewidth=0.5)

    pyplot.xlabel(label_x)
    pyplot.ylabel(label_y)
    pyplot.savefig(path)
    pyplot.close()
