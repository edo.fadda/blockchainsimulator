import csv


def get_data_from_csv(file_name):
    data_csv = {}
    reader = csv.DictReader(open(file_name))
    first = True
    for row in reader:
        if first:
            for r in row:
                data_csv[r] = []
            first = False
        else:
            for key in data_csv.keys():
                data_csv[key].append(row[key])
    return data_csv
