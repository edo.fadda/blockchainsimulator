import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import logging
from plot_result import video

class competing_tipping:
    def __init__(self, graph, thr, set_seed_red, set_seed_blue,
                 delay_red=0, delay_blue=0, set_weight=False):
        """
        set_active_blue: set of all nodes that are active
        """
        logging.info("structure creation")
        self.graph = graph
        self.nodes = self.graph.nodes()
        self.thr = thr
        self.time_step = 0
        self.pos = nx.spring_layout(self.graph)

        self.original_red = list(set_seed_red)[0]
        self.original_blue = list(set_seed_blue)[0]

        self.set_active_red = set_seed_red
        self.set_active_blue = set_seed_blue
        self.set_red = set_seed_red
        self.set_blue = set_seed_blue
        self.dijkstra_matrix = None
        self.delay_red = delay_red
        self.delay_blue = delay_blue
        self.weighted = False

        if set_weight:
            self.weighted = True
            nx.set_node_attributes(self.graph, 'time_colored', -1)
            self.graph.node[self.original_red]['time_colored'] = 0
            self.graph.node[self.original_blue]['time_colored'] = 0
            self.max_length = 0
            for (u, v) in self.graph.edges():
                length = np.random.randint(1, 5)
                if length > self.max_length:
                    self.max_length = length

                self.graph.add_edge(u, v, weight=length)
                logging.info("edge: {} {}".format((u, v),
                                                  self.graph[u][v]['weight']))


    # PERCOLATION DYNAMIC
    def iterate_one(self, print_graphs=True):
        logging.info("\n \n Executing time: {}".format(self.time_step))
        active = set()
        active = active.union(self.set_red, self.set_blue)

        has_changed = False

        set_red_new = set()
        set_blue_new = set()
        logging.info("active {} nodes".format(len(active)))
        logging.info("red node: {}".format(self.set_red))
        logging.info("blue node: {}".format(self.set_blue))

        for n in filter(lambda n: n not in active, self.nodes):
            neighbors = list(self.graph.neighbors(n))
            logging.info("neighbors di {}: \n {}".format(n, list(neighbors)))

            red_neighbors = [n for n in neighbors if n in self.set_red]
            blue_neihbors = [n for n in neighbors if n in self.set_blue]
            logging.info("\t red {}: ".format(list(red_neighbors)))
            logging.info("\t blue {}: ".format(list(blue_neihbors)))

            n_red_neighbors = len(list(red_neighbors))
            n_blue_neihbors = len(list(blue_neihbors))
            h = n_red_neighbors - n_blue_neihbors
            logging.info("h: {}".format(h))

            if h > self.thr and self.time_step >= self.delay_red:
                logging.info("node {} becomes red \n".format(n))
                set_red_new.add(n)
                has_changed = True
            elif h < -self.thr and self.time_step >= self.delay_blue:
                logging.info("node {} becomes blue \n".format(n))
                set_blue_new.add(n)
                has_changed = True
            else:
                logging.info("\n")

        if print_graphs:
            self.__update_color()

        logging.info("--> {}".format(has_changed))

        if has_changed:
            self.set_red = set_red_new.union(self.set_red)
            self.set_blue = set_blue_new.union(self.set_blue)

        logging.info("\n")
        return has_changed

    def iterate_one_weighted(self, print_graphs=True):
        logging.info("\n \n *** Executing time: {} *** \n".format(self.time_step))
        active = set()
        active = active.union(self.set_red, self.set_blue)

        has_changed = False

        set_red_new = set()
        set_blue_new = set()
        logging.info("Active {} nodes".format(len(active)))
        logging.info("Red node: {}".format(self.set_red))
        logging.info("Blue node: {}".format(self.set_blue))
        not_active = [n for n in self.nodes if n not in active]

        for n in not_active:
            neighbors = self.graph.neighbors(n)
            logging.info("\n")
            logging.info("\t neighbors of {}: \t {}".format(n, neighbors))

            red_neighbors = [n for n in neighbors if n in self.set_red]
            blue_neighbors = [n for n in neighbors if n in self.set_blue]

            logging.info("\t red {}: ".format(red_neighbors))
            logging.info("\t blue {}: ".format(blue_neighbors))

            n_red_neighbors = 0
            for n_r in red_neighbors:
                dist = self.time_step - self.graph.node[n]['time_colored']
                logging.info("\t dist from {} : {} - {} || {}".format(n_r, self.time_step, self.graph.node[n_r]['time_colored'],self.graph[n][n_r]['weight']))
                if self.time_step - self.graph.node[n_r]['time_colored'] == self.graph[n][n_r]['weight']:
                    logging.info("\t reached".format(n_r, dist))
                    n_red_neighbors += 1

            n_blue_neighbors = 0
            for n_b in blue_neighbors:
                dist = self.time_step - self.graph.node[n]['time_colored']
                logging.info("\t dist from {} : {} - {} || {} ".format(n_b, self.time_step, self.graph.node[n_b]['time_colored'], self.graph[n][n_b]['weight']))
                if self.time_step - self.graph.node[n_b]['time_colored'] == self.graph[n][n_b]['weight']:
                    logging.info("\t reached")
                    n_blue_neighbors += 1

            h = n_red_neighbors - n_blue_neighbors
            logging.info("\t h: {}".format(h))

            if h > self.thr and self.time_step >= self.delay_red:
                logging.info("\t node {} becomes red".format(n))
                set_red_new.add(n)
                has_changed = True
                self.graph.node[n]['time_colored'] = self.time_step
            elif h < -self.thr and self.time_step >= self.delay_blue:
                logging.info("\t node {} becomes blue".format(n))
                set_blue_new.add(n)
                has_changed = True
                self.graph.node[n]['time_colored'] = self.time_step

        logging.info("--> {}".format(has_changed))

        if has_changed:
            self.set_red = set_red_new.union(self.set_red)
            self.set_blue = set_blue_new.union(self.set_blue)

        if print_graphs:
            self.__update_color()

        logging.info("\n")
        return has_changed

    def run(self, print_graphs=True):
        has_changed = True
        if not self.weighted:
            while has_changed:
                has_changed = self.iterate_one()
                if print_graphs:
                    self._plot_img()
                self.time_step += 1
        else:
            while has_changed:
                for i in range(self.max_length + 1):
                    has_changed = self.iterate_one_weighted()
                    if print_graphs:
                        logging.info(
                            "---> Printing graph {}".format(
                                self.time_step
                            )
                        )
                        self._plot_img()
                    if len(list(self.set_red)) + len(list(self.set_blue)) == len(list(self.nodes)):
                        return self.set_red, self.set_blue
                        break
                    self.time_step += 1

        return self.set_red, self.set_blue

    def set_delay(self, node, timestep):
        self.delay["Node"] = timestep


    # PROPERIES
    def get_closeness_centrality(self):
        logging.info("Computing closeness centrality")
        nx.closeness_centrality(self.graph)

    def get_n_conn_red(self):
        neighbors = list(self.graph.neighbors(self.original_red))
        return len(neighbors)

    def get_n_conn_blue(self):
        neighbors = list(self.graph.neighbors(self.original_blue))
        return len(neighbors)

    def get_average_neighbor_degree_red(self):
        neighbors = list(self.graph.neighbors(self.original_red))
        tot = 0
        for n in neighbors:
            tot += len(list(self.graph.neighbors(n)))
        return tot / len(neighbors)

    def get_average_neighbor_degree_blue(self):
        neighbors = list(self.graph.neighbors(self.original_blue))
        tot = 0
        for n in neighbors:
            tot += len(list(self.graph.neighbors(n)))
        return tot / len(neighbors)

    def get_n_conn_red_2(self):
        neighbors = self.graph.neighbors(self.original_red)
        tot = 0
        for n in neighbors:
            x = list(self.graph.neighbors(n))
            tot += len(list(x))
        return tot

    def get_n_conn_blue_2(self):
        neighbors = self.graph.neighbors(self.original_blue)
        tot = 0
        for n in neighbors:
            x = list(self.graph.neighbors(n))
            tot += len(list(x))
        return tot

    # DISTANCE MATRIX
    def get_dijkstra_matrix(self):
        if type(self.dijkstra_matrix) is not np.ndarray:
            logging.info("Computing Dijkstra matrix")
            self.dijkstra_matrix = nx.floyd_warshall_numpy(self.graph)
        return self.dijkstra_matrix

    def get_distances_from_blue(self):
        if type(self.dijkstra_matrix) is not np.ndarray:
            logging.info("Computing Dijkstra matrix")
            self.dijkstra_matrix = nx.floyd_warshall_numpy(self.graph)
        return self.dijkstra_matrix[self.original_blue, :]

    def get_distances_from_red(self):
        if type(self.dijkstra_matrix) is not np.ndarray:
            logging.info("Computing Dijkstra matrix")
            self.dijkstra_matrix = nx.floyd_warshall_numpy(self.graph)
        return self.dijkstra_matrix[self.original_red, :]

    def get_max_distances_from_red(self):
        vec = self.get_distances_from_red()
        return vec.max()

    def get_max_distances_from_blue(self):
        vec = self.get_distances_from_blue()
        return vec.max()

    def get_eigen_values_distance(self):
        if type(self.dijkstra_matrix) is not np.ndarray:
                logging.info("Computing Dijkstra matrix")
                self.dijkstra_matrix = nx.floyd_warshall_numpy(self.graph)
        eigs = np.linalg.eig(self.dijkstra_matrix)
        return eigs[0]


    # PLOT
    def print_graph(self):
        for n in self.nodes:
            neighbors = self.graph.neighbors(n)
            print("neighbors of {} are: {}".format(n, neighbors))

    def print_edges(self):
        for (u, v) in self.graph.edges():
            print("{} {}".format((u, v), self.graph[u][v]['weight']))

    def _plot_img(self, show=False):
        plt.axis('off')
        label = "t: {}".format(self.time_step)
        plt.text(1, 0.1, label, ha='right', va='top')
        plt.savefig("img/Graph_{}.png".format(self.time_step), dpi=100)
        if show:
            plt.show()
        plt.close()

    def get_size_red(self):
        return len(list(self.set_red))

    def get_size_blue(self):
        return len(list(self.set_blue))

    def plot_graph(self, label=True, grey=True):
        plt.axis('off')
        self.__update_color(label, grey)
        plt.savefig("img/Graph.png")

    def dump_video(self):
        filenames = []
        for i in range(self.time_step):
            filenames.append("img/Graph_" + str(i) + '.png')
        video.produce_video_long(filenames, 2, "img\\")

    def __update_color(self, label=False, grey=False):
        nx.draw_networkx_nodes(
            self.graph, self.pos,
            nodelist=list(self.set_red),
            node_color='r',
            node_size=50,
            alpha=0.8)

        nx.draw_networkx_nodes(
            self.graph, self.pos,
            nodelist=list(self.set_blue),
            node_color='b',
            node_size=50,
            alpha=0.8)

        red_and_blue = self.set_blue.union(self.set_red)
        normal_nodes = set(self.nodes).difference(red_and_blue)
        if grey:
            nx.draw_networkx_nodes(
                self.graph, self.pos,
                nodelist=list(normal_nodes),
                with_labels=True,
                node_color='grey',
                node_size=50,
                alpha=0.8)
        else:
            nx.draw_networkx_nodes(
                self.graph, self.pos,
                nodelist=list(normal_nodes),
                with_labels=True,
                node_color='black',
                node_size=50,
                alpha=0.8)

        nx.draw_networkx_edges(
            self.graph, self.pos,
            width=0.8, alpha=0.5,
            edge_color='grey')
        if label:
            nx.draw_networkx_labels(
                self.graph, self.pos,
                font_size=8, font_family='sans-serif')
        if self.weighted:
            edge_labels = nx.get_edge_attributes(self.graph, 'weight')
            nx.draw_networkx_edge_labels(
                self.graph,
                pos=self.pos,
                edge_labels=edge_labels
            )
