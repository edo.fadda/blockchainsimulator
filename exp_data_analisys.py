import networkx as nx
import numpy as np
import sys
import os
import random
import operator
import logging
import pickle

from plot_result import graphs, read_data
from competing_tipping import competing_tipping


log_name = "logs/exp_data_analysis.log"
logging.basicConfig(filename=log_name,
                    format='%(asctime)s %(levelname)s: %(message)s',
                    level=logging.INFO, datefmt="%H:%M:%S",
                    filemode='w')

n_node = 1000
p = 0.1
N_EXP = 10
file_name = './exp_file.csv'

compute = True

if compute:
    if os.path.exists(file_name):
        append_write = 'a'
    else:
        append_write = 'w'

    f1 = open(file_name, append_write)

    if append_write == 'w':
        distances = "max_dist_red,max_dist_blue,max_eig,min_eig"
        connectivity = "n_conn_red,n_conn_blue,n_conn_red_2,n_conn_blue_2,n_ave_nei_deg_red,n_ave_nei_deg_blue"
        components = "time_steps,size_red,size_blue"
        f1.write("n_node,p,{},{},{} \n".format(distances, connectivity, components))

    for i in range(N_EXP):
        print("{}/{}".format(i, N_EXP))
        g = nx.erdos_renyi_graph(n_node, p)
        seed_set_red = {0}
        seed_set_blue = {n_node - 1}
        consensus = competing_tipping.competing_tipping(g, 0,
                                                        seed_set_red,
                                                        seed_set_blue, 0, 0)

        active_red, active_blue = consensus.run(print_graphs=False)

        time_steps = consensus.time_step

        vector = consensus.get_distances_from_red()

        max_dist_red = consensus.get_max_distances_from_red()
        max_dist_blue = consensus.get_max_distances_from_blue()
        eigs = consensus.get_eigen_values_distance()
        distances = "{},{},{},{}".format(max_dist_red, max_dist_blue,
                                         max(abs(eigs)), min(abs(eigs)))

        # CONNECTIVITY:
        n_conn_red = consensus.get_n_conn_red()
        n_conn_blue = consensus.get_n_conn_blue()

        n_conn_red_2 = consensus.get_n_conn_red_2()
        n_conn_blue_2 = consensus.get_n_conn_blue_2()

        n_ave_nei_deg_red = consensus.get_average_neighbor_degree_red()
        n_ave_nei_deg_blue = consensus.get_average_neighbor_degree_blue()
        connectivity = "{},{},{},{},{},{}".format(n_conn_red, n_conn_blue,
                                                  n_conn_red_2, n_conn_blue_2,
                                                  n_ave_nei_deg_red,
                                                  n_ave_nei_deg_blue)

        # COMPONENTS
        components = "{},{},{}".format(time_steps, len(active_red),
                                       len(active_blue))

        f1.write("{},{},{},{},{} \n".format(n_node,
                                            p,
                                            distances,
                                            connectivity,
                                            components
                                            ))

    f1.close()
else:
    import math
    file_name = './exp_file_1.csv'
    dict_data = read_data.get_data_from_csv(file_name)
    x = []
    y = []
    for i in range(len(dict_data['n_conn_red'])):
        x.append(float(dict_data['n_conn_red'][i]) - float(dict_data['n_conn_blue'][i]))
        y.append(float(dict_data['size_red'][i]) - float(dict_data['size_blue'][i]))
    graphs.plot_scatter_x_y(x, 'diff degree', y, 'diff size',
                    "","img\Diff_{}_{}.png".format(n_node, p))
# index, value = max(enumerate({lr,lb}), key=operator.itemgetter(1))
# print("{},{},{}".format(index, lr, lb))

'''

if index == 0:
    c_max = cc[list(seed_set_red)[0]]
    c_min = cc[list(seed_set_blue)[0]]
else:
    c_max = cc[list(seed_set_blue)[0]]
    c_min = cc[list(seed_set_red)[0]]

D = 0
for k in range(0,99):
    nx.shortest_path(g,list(seed_set_red)[0],k)
    D = D + np.sign( nx.shortest_path_length(g,list(seed_set_red)[0],k) - nx.shortest_path_length(g,list(seed_set_blue)[0],k))

if index == 1:
    D = -D

'''
