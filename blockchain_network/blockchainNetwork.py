# -*- coding: utf-8 -*-
import random
import logging
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt


def most_frequent(List):
    return max(set(List), key=List.count)


class BlockchainNetwork():
    def __init__(self, n_node, p_edge, p_back_bone, time_limit, p_mining):
        logging.info("#############")
        logging.info("#### NETWORK CREATION ####")
        logging.info("#############")
        self.G = nx.erdos_renyi_graph(n_node, p_edge)

        cc = nx.closeness_centrality(self.G)
        cc_l = []
        for key, ele in cc.iteritems():
            cc_l.append(ele)
        self.cc = np.array(cc_l, dtype=float)
        max_cc = np.max(self.cc)
        max_cc_pos = np.where(self.cc == max_cc)[0][0]
        logging.info("\t max closeness_centrality: {} [{}]".format(
            max_cc,
            max_cc_pos)
        )
        min_cc = np.min(self.cc)
        min_cc_pos = np.where(self.cc == min_cc)[0][0]
        logging.info("\t min closeness_centrality: {} [{}]".format(
            np.min(self.cc),
            min_cc_pos)
        )
        self.cc_stats = [
            max_cc,
            min_cc,
            np.mean(self.cc),
            np.std(self.cc),
        ]

        self.pos = nx.spring_layout(self.G)
        self.cpu_power_lst = np.random.rand(n_node)
        self.cpu_power_lst = self.cpu_power_lst / sum(self.cpu_power_lst)
        self.n_node = n_node
        self.time_limit = time_limit
        self.p_mining = p_mining
        self.color_list = [
            'green',
            'blue',
            'red',
            'gray',
            'pink',
            'brown',
            'purple'
        ]
        cpu_powers = {}
        back_bones = {}
        color_nodes = {}
        last_blocks = {}
        msgs_node = {}

        sum_back_bones = 0
        for i in range(n_node):
            cpu_powers[i] = self.cpu_power_lst[i]
            msgs_node[i] = []
            back_bones[i] = np.random.binomial(1, p_back_bone, 1)[0]
            last_blocks[i] = "0-origin"
            if back_bones[i] == 1:
                logging.info(
                    "\t node: {idx} [cpu power: {cpu:.2f}] last_block: {blc} is in the backbone".format(
                        idx=i, cpu=cpu_powers[i], blc=last_blocks[i])
                )
                sum_back_bones += 1
            else:
                logging.info(
                    "\t node: {idx} [cpu power: {cpu:.2f}] last_block: {blc}".format(
                        idx=i, cpu=cpu_powers[i], blc=last_blocks[i])
                )
            if back_bones[i] == 1:
                color_nodes[i] = 'g'
            else:
                color_nodes[i] = 'orange'

        nx.set_node_attributes(self.G, cpu_powers, 'cpu_power')
        nx.set_node_attributes(self.G, last_blocks, 'last_block')
        nx.set_node_attributes(self.G, back_bones, 'is_back_bone')
        nx.set_node_attributes(self.G, color_nodes, 'color')
        nx.set_node_attributes(self.G, msgs_node, 'msg')

        latencies = {}
        colors_edge = {}
        info_flowing = {}
        for e in self.G.edges:
            both_bb = self.G.node[e[0]]['is_back_bone'] * self.G.node[e[1]]['is_back_bone']
            if (both_bb | (sum_back_bones == 0)):
                latencies[e] = 1
                colors_edge[e] = 'g'
            else:
                latencies[e] = 2
                colors_edge[e] = 'orange'
            info_flowing[e] = {}
            info_flowing[e][e[0]] = [0] * latencies[e]
            info_flowing[e][e[1]] = [0] * latencies[e]

        nx.set_edge_attributes(self.G, latencies, 'latency')
        nx.set_edge_attributes(self.G, colors_edge, 'color')
        nx.set_edge_attributes(self.G, info_flowing, 'msg')

        for e in self.G.edges:
            logging.info("link: {} has speed: {}".format(
                e,
                self.G.edges[e]['latency'])
            )

    def plot_graph(self):
        nodes = self.G.nodes()
        node_colors = [self.G.node[n]['color'] for n in nodes]
        edge_colors = [self.G[u][v]['color'] for u, v in self.G.edges]
        nx.draw(
            self.G,
            edge_color=edge_colors,
            nodelist=nodes,
            node_color=node_colors,
            # labels=labeldict,
            with_labels=True
        )
        plt.show()
        plt.close()

    def plot_hist_comp_power(self):
        plt.hist(self.cpu_power_lst, bins='auto')
        plt.title("Histogram of computational power")
        # plt.show()
        plt.savefig('./img/comp_power_th.png')
        plt.close()

    def plot_graph_last_block(self, idx):
        for n in self.G.nodes:
            self.G.node[n]['color'] = 'blue'
        nodes = self.G.nodes()

        dict_blocks_color = {}
        dict_blocks_color['0-origin'] = 'gray'
        possible_blocks = ['0-origin']
        dict_color_nodes = {}
        dict_color_nodes['gray'] = []

        for n in self.G.nodes:
            if self.G.node[n]['last_block'] not in possible_blocks:
                possible_blocks.append(self.G.node[n]['last_block'])
        possible_blocks.sort()
        for i, block in enumerate(possible_blocks):
            if block == '0-origin':
                continue
            dict_blocks_color[block] = self.color_list[i]
            dict_color_nodes[self.color_list[i]] = []

        for n in self.G.nodes:
            color = dict_blocks_color[self.G.node[n]['last_block']]
            dict_color_nodes[color].append(n)

        node_colors = []
        for n in nodes:
            node_colors.append(dict_blocks_color[self.G.node[n]['last_block']])

        edge_colors = [self.G[u][v]['color'] for u, v in self.G.edges]

        for key, ele in dict_color_nodes.iteritems():
            if len(ele) == 0:
                continue
            last_block_color = self.G.node[ele[0]]['last_block']
            nx.draw_networkx_nodes(
                self.G,
                pos=self.pos,
                nodelist=ele,
                node_color=key,
                label=last_block_color,
            )
        # labels
        labels = dict()
        for i in nodes:
            labels[i] = str(i)
        nx.draw_networkx_labels(self.G, self.pos, labels)
        # edges
        nx.draw_networkx_edges(
            self.G, pos=self.pos, edge_color=edge_colors
        )
        plt.title("t: {}".format(idx))
        plt.legend()
        # plt.show()
        plt.savefig(
            './img/competing_t_{}.png'.format(
                idx
            )
        )
        plt.close()

    def has_fork(self, threshold=10):
        values = set()
        last_block_lst = []
        for n in self.G.nodes:
            values.add(self.G.node[n]['last_block'])
            last_block_lst.append(self.G.node[n]['last_block'])
        # plt.hist(last_block_lst)
        # plt.show()
        logging.info("block; occurency")
        for ele in values:
            logging.info("{}: {}".format(
                ele,
                last_block_lst.count(ele))
            )

    def is_connected(self):
        return nx.is_connected(self.G)

    def consensus(self, threshold):
        possible_blocks = {}
        for n in self.G.nodes:
            if self.G.node[n]['last_block'] not in possible_blocks:
                possible_blocks[self.G.node[n]['last_block']] = 1
            else:
                possible_blocks[self.G.node[n]['last_block']] += 1
        size_fork = 0
        for key, ele in possible_blocks.iteritems():
            if ele > threshold * self.n_node:
                return True, key
            else:
                size_fork = ele
        return False, max(size_fork, 100 - size_fork)

    def block_propagation(self, actual_time, plot_graphs=True):
        # each node send a msg to its neiborhood
        # the msg can be ignored
        consider_first_arrival = False
        random_arrival = True

        for n in self.G.nodes:
            for ele in self.G.neighbors(n):
                # the msg that is sent:
                self.G.edges[(n, ele)]['msg'][ele].insert(
                    0,
                    self.G.node[n]['last_block']
                )
                # the msg that arrives:
                last_msg = self.G.edges[(n, ele)]['msg'][ele].pop(-1)
                logging.info("link from {} to {}: {}".format(
                    n,
                    ele,
                    self.G.edges[(n, ele)]['msg'][ele])
                )
                if last_msg == 0:
                    continue
                else:
                    self.G.node[ele]['msg'].append(
                        self.G.node[n]['last_block']
                    )

        for n in self.G.nodes:
            logging.info("\t miner {} [{}] msgs received:".format(
                n,
                self.G.node[n]['last_block'])
            )
            last_nbm = self.G.node[n]['last_block'].split('-')[0]
            if len(self.G.node[n]['msg']) == 0:
                return

            if consider_first_arrival:
                # it considers the first node in the pile
                # whith a block after the last known
                for msg in self.G.node[n]['msg']:
                    logging.info("\t\t {}".format(
                        msg)
                    )
                    msg_nbm = msg.split('-')[0]
                    if last_nbm < msg_nbm:
                        logging.info("\t\t\t NEW BLOCK")
                        self.G.node[n]['last_block'] = msg
                        last_nbm = msg_nbm

            if random_arrival:
                # it considers a random node
                # between all the new one
                new_blocks = []
                same_height = []
                for msg in self.G.node[n]['msg']:
                    logging.info("\t\t {}".format(
                        msg)
                    )
                    msg_nbm = msg.split('-')[0]
                    if last_nbm < msg_nbm:
                        new_blocks.append(msg)
                    if last_nbm == msg_nbm:
                        same_height.append(msg)
                if len(new_blocks) > 0:
                    self.G.node[n]['last_block'] = random.choice(new_blocks)
                    logging.info("\t\t\t NEW BLOCK: {}".format(
                        self.G.node[n]['last_block'])
                    )
                else:
                    if len(same_height) > 0:
                        logging.info(same_height)
                        same_height.append(self.G.node[n]['last_block'])
                        mode_block = most_frequent(same_height)
                        if (mode_block != self.G.node[n]['last_block']) & (same_height.count(mode_block) > 1):
                            self.G.node[n]['last_block'] = mode_block
                            logging.info("\t\t\t NEW BLOCK MODE: {}".format(
                                self.G.node[n]['last_block'])
                            )
            self.G.node[n]['msg'] = []

    def load_simulation(self):
        blocks_time = []
        for t in range(self.time_limit):
            blocks_time.append(
                np.random.poisson(self.p_mining, 1)[0]
            )
        n_blocks = sum(blocks_time)

        logging.info("Number of blocks mined: {}".format(
            n_blocks
        ))
        logging.info("\t blocks mined: {}".format(
            blocks_time
        ))

        # first mining
        winning_miners = np.random.choice(
            self.n_node,
            n_blocks,
            p=self.cpu_power_lst
        )
        logging.info("The winning miners are: {}".format(
            winning_miners)
        )

        self.winning_miners_time = []
        count = 0
        for t in range(self.time_limit):
            if blocks_time[t] != 0:
                self.winning_miners_time.append([])
            else:
                self.winning_miners_time.append(
                    winning_miners[count:count+blocks_time[t]]
                )
                count += blocks_time[t]
        logging.info("The winning miners for time: {}".format(
            self.winning_miners_time)
        )
        # Check if empirical frequency is similar to the
        # cpu powers Selection process
        # unique, counts = np.unique(winning_miners, return_counts=True)
        # for i, ele in enumerate(unique):
        #     print("{}: {} [th: {}]".format(
        #         ele,
        #         (counts[i] + 0.0) / (1000.0),
        #         cpu_power_lst[i]
        #     ))

    def set_winning_miners(self, list_of_miners, time_limit):
        self.winning_miners_time = []
        for t in range(time_limit):
            if t == 0:
                self.winning_miners_time.append(list_of_miners)
            else:
                self.winning_miners_time.append([])

    def set_winning_miners_time(self, dict_of_miners, time_limit):
        self.winning_miners_time = []
        for t in range(time_limit):
            if t in dict_of_miners:
                self.winning_miners_time.append(dict_of_miners[t])
            else:
                self.winning_miners_time.append([])

    def run_simulation(self, time_horizon, plot_graph=False):
        for t in range(time_horizon):
            print(">>>>>>> TIME {}".format(t))
            logging.info(">>>>>>> TIME {}".format(t))
            logging.info("\t MINERS WIN: {}".format(
                self.winning_miners_time[t])
            )
            winning_miners = self.winning_miners_time[t]
            for wm in winning_miners:
                last_block = self.G.node[wm]['last_block'].split('-')[0]
                self.G.node[wm]['last_block'] = "{}-{}".format(
                    int(last_block) + 1,
                    wm
                )
            if plot_graph:
                self.plot_graph_last_block(t)
            self.block_propagation(self.G, t)
            self.has_fork(self.G)
            logging.info("\n")

    def log_graph(self):
        logging.info(">>>>>>> LOG WHOLE GRAPH:")
        for n in self.G.nodes:
            neighbors = []
            for n1 in self.G.neighbors(n):
                neighbors.append("{}:{}".format(
                    n1, self.G.node[n1]['last_block'])
                )
            logging.info("{} [{}]".format(
                n, ",".join(neighbors))
            )

    def property_node(self, idx):
        ans = []
        # number of neighbors
        n_neighbors = 0
        neighbors_2 = []
        for n in self.G.neighbors(idx):
            n_neighbors += 1
            # 1st level neighbors
            if n not in neighbors_2:
                neighbors_2.append(n)
            # 2nd level neighbors
            for n1 in self.G.neighbors(n):
                if n1 not in neighbors_2:
                    neighbors_2.append(n1)
        ans.append(n_neighbors)
        ans.append(len(neighbors_2))
        sp_list = []
        sp = nx.shortest_path_length(self.G, idx)
        for key, ele in sp.iteritems():
            if key == idx:
                continue
            sp_list.append(ele)

        sp_list = np.asarray(sp_list)
        print(sp_list)
        ans.append(np.mean(sp_list))
        ans.append(np.max(sp_list))
        ans.append(np.median(sp_list))
        ans.append(np.std(sp_list))
        ans.append(nx.closeness_centrality(self.G, idx))
        return ans

    def get_cc_stats(self):
        return self.cc_stats

    def get_cdf_cc_node(self, idx):
        cc_idx = self.cc[idx]
        min_ele = 0
        for cc in self.cc:
            if cc < cc_idx:
                min_ele += 1
        return float(min_ele) / float(len(self.cc))
