# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt


if __name__ == '__main__':
    log_name = "logs/cc_vs_nblock_analysis.log"
    df = pd.read_csv(
        './results/test_cc_vs_n_block.csv',
        sep=';',
        header=0
    )
    path_fig = None

    # ###############
    # diff-cc vs p_winning
    # ###############
    df_conv = df.loc[(df['conv'] == 'conv')]
    ele = []
    for i in range(10):
        ele.append((i + 0.0) / 10.0)
    '''
    x_val = []
    y_val = []
    for i in range(1, len(ele)):
        min_val = ele[i-1]
        max_val = ele[i]
        n_quant_0 = len(df_conv.loc[(df_conv['q_node_0'] > min_val) & (df_conv['q_node_0'] <= max_val)])
        n_quant_0_winning = len(df_conv.loc[(df_conv['q_node_0'] > min_val) & (df_conv['q_node_0'] <= max_val) & (df_conv['det'] == 0)])
        n_quant_1 = len(df_conv.loc[(df_conv['q_node_1'] > min_val) & (df_conv['q_node_1'] <= max_val)])
        n_quant_1_winning = len(df_conv.loc[(df_conv['q_node_1'] > min_val) & (df_conv['q_node_1'] <= max_val) & (df_conv['det'] == 1)])
        ris = (n_quant_0_winning + n_quant_1_winning + 0.0) / (n_quant_0 + n_quant_1 + 0.0)
        x_val.append(min_val)
        y_val.append(ris)
        x_val.append(max_val)
        y_val.append(ris)

    plt.plot(
        x_val, y_val,
        color="green"
    )
    
    plt.title("Probability of winning vs cc")
    plt.xlabel('quantile cc')
    plt.ylabel('freq victory')
    if path_fig:
        plt.savefig(
            '{}/cc_vs_wining.png'.format(
                path_fig
            )
        )
    else:
        plt.show()
    plt.close()
    '''
    '''
    # ###############
    # p-conv vs p_edge
    # ###############
    p_values = df.p.unique()
    p_conv = []
    for p_val in p_values:
        n_conv = len(df.loc[(df['conv'] == 'conv') & (df['p'] == p_val)])
        tot = len(df.loc[df['p'] == p_val])
        p_conv.append(
            float(n_conv) / float(tot)
        )
    # plt.scatter(p_values, p_conv)
    plt.plot(p_values, p_conv, color="blue")

    plt.title("Probability of convergence")
    plt.xlabel('p edge')
    plt.ylabel('p convergence')
    if path_fig:
        plt.savefig(
            '{}/convergence.png'.format(
                path_fig
            )
        )
    else:
        plt.show()
    plt.close()
    '''
