# -*- coding: utf-8 -*-
import logging
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt


def has_fork(G, threshold=10):
    values = set()
    last_block_lst = []
    for n in G.nodes:
        values.add(G.node[n]['last_block'])
        last_block_lst.append(G.node[n]['last_block'])
    # plt.hist(last_block_lst)
    # plt.show()
    logging.info("block; occurency")
    for ele in values:
        logging.info("{}: {}".format(
            ele,
            last_block_lst.count(ele))
        )


def block_propagation(G, actual_time, plot_graphs=True):
    # each node send a msg to its neiborhood
    # the msg can be ignored
    for n in G.nodes:
        for ele in G.neighbors(n):
            # the msg that is sent:
            G.edges[(n, ele)]['msg'][ele].insert(
                0,
                G.node[n]['last_block']
            )
            # the msg that arrives:
            last_msg = G.edges[(n, ele)]['msg'][ele].pop(-1)
            logging.info("{} {}: {}".format(
                n,
                ele,
                G.edges[(n, ele)]['msg'][ele])
            )
            if last_msg == 0:
                continue
            else:
                G.node[ele]['msg'].append(
                    G.node[n]['last_block']
                )

    for n in G.nodes:
        logging.info("\t miner {} msgs:".format(
            n)
        )
        last_nbm = G.node[n]['last_block'].split('-')[0]
        for msg in G.node[n]['msg']:
            logging.info("\t\t {}".format(
                msg)
            )
            msg_nbm = msg.split('-')[0]
            if last_nbm < msg_nbm:
                logging.info("\t\t\t NEW BLOCK")
                G.node[n]['last_block'] = msg
                last_nbm = msg_nbm
        G.node[n]['msg'] = []

    if plot_graphs:
        for n in G.nodes:
            G.node[n]['color'] = 'blue'
        nodes = G.nodes()
        node_colors = [G.node[n]['color'] for n in nodes]
        edge_colors = [G[u][v]['color'] for u, v in G.edges]
        nx.draw(
            G,
            edge_color=edge_colors,
            nodelist=nodes,
            node_color=node_colors
        )
        plt.show()
        plt.close()


if __name__ == '__main__':
    log_name = "logs/cpu_fork_relation.log"
    logging.basicConfig(
        filename=log_name,
        format='%(asctime)s %(levelname)s: %(message)s',
        level=logging.INFO, datefmt="%H:%M:%S",
        filemode='w'
    )
    plot_graphs = False

    n_node = 5
    p_edge = 0.9
    p_back_bone = 0.3
    time_limit = 20
    p_mining = 0.5

    logging.info("#############")
    logging.info("#### NETWORK CREATION ####")
    logging.info("#############")
    G = nx.erdos_renyi_graph(n_node, p_edge)

    cpu_power_lst = np.random.rand(n_node)
    cpu_power_lst = cpu_power_lst / sum(cpu_power_lst)
    if plot_graphs:
        plt.hist(cpu_power_lst, bins='auto')
        plt.title("Histogram of computational power")
        # plt.show()
        plt.savefig('./img/comp_power_th.png')
        plt.close()

    cpu_powers = {}
    back_bones = {}
    color_nodes = {}
    last_blocks = {}
    msgs_node = {}
    for i in range(n_node):
        cpu_powers[i] = cpu_power_lst[i]
        msgs_node[i] = []
        back_bones[i] = np.random.binomial(1, p_back_bone, 1)[0]
        last_blocks[i] = "0-origin"
        if back_bones[i] == 1:
            logging.info(
                "\t node: {idx} [cpu power: {cpu:.2f}] last_block: {blc} is in the backbone".format(
                    idx=i, cpu=cpu_powers[i], blc=last_blocks[i])
            )
        else:
            logging.info(
                "\t node: {idx} [cpu power: {cpu:.2f}] last_block: {blc}".format(
                    idx=i, cpu=cpu_powers[i], blc=last_blocks[i])
            )

        if back_bones[i] == 1:
            color_nodes[i] = 'g'
        else:
            color_nodes[i] = 'orange'

    nx.set_node_attributes(G, cpu_powers, 'cpu_power')
    nx.set_node_attributes(G, last_blocks, 'last_block')
    nx.set_node_attributes(G, back_bones, 'is_back_bone')
    nx.set_node_attributes(G, color_nodes, 'color')
    nx.set_node_attributes(G, msgs_node, 'msg')

    latencies = {}
    colors_edge = {}
    info_flowing = {}
    for e in G.edges:
        both_bb = G.node[e[0]]['is_back_bone'] * G.node[e[1]]['is_back_bone']
        if both_bb:
            latencies[e] = 1
            colors_edge[e] = 'g'
        else:
            latencies[e] = 2
            colors_edge[e] = 'orange'
        info_flowing[e] = {}
        info_flowing[e][e[0]] = [0] * latencies[e]
        info_flowing[e][e[1]] = [0] * latencies[e]

    nx.set_edge_attributes(G, latencies, 'latency')
    nx.set_edge_attributes(G, colors_edge, 'color')
    nx.set_edge_attributes(G, info_flowing, 'msg')

    for e in G.edges:
        logging.info("link: {} has speed: {}".format(
            e,
            G.edges[e]['latency'])
        )

    if plot_graphs:
        nodes = G.nodes()
        node_colors = [G.node[n]['color'] for n in nodes]
        edge_colors = [G[u][v]['color'] for u, v in G.edges]
        nx.draw(
            G,
            edge_color=edge_colors,
            nodelist=nodes,
            node_color=node_colors
        )
        plt.show()
        plt.close()

    logging.info("#############")
    logging.info("#### END NETWORK CREATION ####")
    logging.info("#############\n\n\n")

    logging.info("#############")
    logging.info("#### PRE SIMULATION ####")
    logging.info("#############")

    blocks_time = []
    for t in range(time_limit):
        blocks_time.append(
            np.random.poisson(p_mining, 1)[0]
        )
    n_blocks = sum(blocks_time)

    logging.info("Number of blocks mined: {}".format(
        n_blocks
    ))
    logging.info("\t blocks mined: {}".format(
        blocks_time
    ))

    # first mining
    winning_miners = np.random.choice(
        n_node,
        n_blocks,
        p=cpu_power_lst
    )
    logging.info("The winning miners are: {}".format(
        winning_miners)
    )

    winning_miners_time = []
    count = 0
    for t in range(time_limit):
        if blocks_time[t] == 0:
            winning_miners_time.append([])
        else:
            winning_miners_time.append(
                winning_miners[count:count+blocks_time[t]]
            )
            count += blocks_time[t]
    logging.info("The winning miners for time: {}".format(
        winning_miners_time)
    )
    # Check if empirical frequency is similar to the
    # cpu powers Selection process
    # unique, counts = np.unique(winning_miners, return_counts=True)
    # for i, ele in enumerate(unique):
    #     print("{}: {} [th: {}]".format(
    #         ele,
    #         (counts[i] + 0.0) / (1000.0),
    #         cpu_power_lst[i]
    #     ))

    logging.info("#############")
    logging.info("#### END PRE SIMULATION ####")
    logging.info("#############\n\n\n")

    logging.info("#############")
    logging.info("#### START SIMULATION ####")
    logging.info("#############")

    for t in range(5):
        print(">>>>>>> TIME {}".format(t))
        logging.info(">>>>>>> TIME {}".format(t))
        logging.info("\t MINERS WIN: {}".format(
            winning_miners_time[t])
        )
        winning_miners = winning_miners_time[t]
        for wm in winning_miners:
            last_block = G.node[wm]['last_block'].split('-')[0]
            G.node[wm]['last_block'] = "{}-{}".format(
                int(last_block) + 1,
                wm
            )
        block_propagation(G, t)
        has_fork(G)
        logging.info("\n")
